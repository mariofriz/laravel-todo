# Laravel-todo

This is just a small demo of what you can do with Laravel.
Just a very basic todo app with user registration/login and todo task creation/deletion.

Backed by a sqlite database.
You need to run the migrations before the applications works:

    php artisan migrate:install //If not installed yet
    php artisan migrate