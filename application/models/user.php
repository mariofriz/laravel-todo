<?php

class User extends Eloquent {

    public static $timestamps = true;
    
    public function todos()
    {
        return $this->has_many('Todo');
    }

}