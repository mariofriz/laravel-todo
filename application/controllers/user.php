<?php

class User_Controller extends Base_Controller {

    public function action_login()
    {
        $input = Input::get();
        // Define the rules for the login form
        $rules = array(
            'email' => 'required|email',
            'password' => 'required',
        );
        $validation = Validator::make($input, $rules);
        // Test if the login form was valid, if it doesn't validat redirect to the form page
        if ($validation->fails())
        {
            return Redirect::to_action('home@index')->with_errors($validation);
        }
        
        $credentials = array('username' => $input['email'], 'password' => $input['password']);
        
        // Try to login with the provided data, if it works list the user's todos. else redirect to the login page
        if (Auth::attempt($credentials))
        {
            // Redirect::to_action(controller_name@function_name)
            return Redirect::to_action('todo@index');
        }
        else
        {
            return Redirect::to_action('home@index');
        }
    }

    public function action_signup()
    {
        $input = Input::get();
        $rules = array(
            'email' => 'required|email|unique:users,email',
            'password' => 'required',
            'password-repeat' => 'same:password',
        );
        $validation = Validator::make($input, $rules);
        if ($validation->fails())
        {
            return Redirect::to_action('home@index')->with_errors($validation);
        }
        $user = new User;
        $user->email = $input['email'];
        $password = Hash::make($input['password']);
        $user->password = $password;
        $user->save();
        return Redirect::to_action('todo@index');
    }
    
    public function action_logout()
    {
        Auth::logout();
        return Redirect::to_action('home@index');
    }

}
