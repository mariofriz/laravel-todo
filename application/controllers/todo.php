<?php

class Todo_Controller extends Base_Controller {

    public function __construct()
    {
        $this->filter('before', 'auth');
    }
        
    public function action_index()
    {  
        $user = Auth::user();
        $todos = $user->todos;
        $parameters = array(
            'todos' => $todos,
            'user' => $user,
        );
        return View::make('todo.index', $parameters);
    }
    
    public function action_add()
    {
        $input = Input::get();
        $rules = array(
            'todo' => 'required',
        );
        $validation = Validator::make($input, $rules);
        if ($validation->fails())
        {
            return Redirect::to_action('todo@index')->with_errors($validation);
        }
        $todo = new Todo;
        $todo->user_id = Auth::user()->id;
        $todo->content = $input['todo'];
        var_dump($todo);
        $todo->save();
        return Redirect::to_action('todo@index');
    }
    
    public function action_delete($id = null)
    {   
        if(!is_null($id) && $todo = Todo::find($id))
        {
            $todo->delete();
        }
        return Redirect::to_action('todo@index');
    }

}