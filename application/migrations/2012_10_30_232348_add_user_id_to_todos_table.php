<?php

class Add_User_Id_To_Todos_Table {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('todos', function($table){
            $table->integer('user_id');
        });		
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('todos', function($table){
            $table->drop_column('user_id');
        });
	}

}