<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Laravel-todo</title>
        <meta name="viewport" content="width=device-width">
        {{ HTML::style('laravel/css/style.css') }}
        {{ HTML::style('css/bootstrap.min.css') }}
    </head>
    <body>
        <div class="wrapper">
            <header>
                <h1>Laravel-todo</h1>
                <h2>A simple todo app built with laravel</h2>
            </header>
            <div role="main" class="main">
                <div class="home">
                    
                    <h2>Login</h2>
                    
                    @if ( $errors )
                        <div class="alert alert-error">
                            <strong>Warning!</strong> Best check yo self, you're not looking too good.
                        </div>
                    @endif

                    {{ Form::open('user/login', 'POST', array('class' => 'form-horizontal')) }}
                    <p>
                        {{ Form::label('email', 'E-Mail Address:') }}
                        {{ Form::text('email') }}
                    </p>
                    <p>
                        {{ Form::label('password', 'Password:') }}
                        {{ Form::password('password') }}
                    </p>
                    <p>
                        {{ Form::submit('Login', array('class' => 'btn')) }}
                    </p>
                    {{ Form::close() }}

                    <h2>Sign up</h2>
                    
                    @if ( $errors )
                        <div class="alert alert-error">
                            <strong>Warning!</strong> Best check yo self, you're not looking too good.
                        </div>
                    @endif

                    {{ Form::open('user/signup', 'POST', array('class' => 'form-horizontal')) }}
                    <p>
                        {{ Form::label('email', 'E-Mail Address:') }}
                        {{ Form::text('email') }}
                    </p>
                    <p>
                        {{ Form::label('password', 'Password:') }}
                        {{ Form::password('password') }}
                    </p>
                    <p>
                        {{ Form::label('password-repeat', 'Password again:') }}
                        {{ Form::password('password-repeat') }}
                    </p>
                    <p>
                        {{ Form::submit('Sign up', array('class' => 'btn')) }}
                    </p>
                    {{ Form::close() }}

                    <ul class="out-links">
                        <li><a href="http://laravel.com">Official Website</a></li>
                        <li><a href="http://forums.laravel.com">Laravel Forums</a></li>
                        <li><a href="http://github.com/laravel/laravel">GitHub Repository</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </body>
</html>
