<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Laravel-todo</title>
        <meta name="viewport" content="width=device-width">
        {{ HTML::style('laravel/css/style.css') }}
        {{ HTML::style('css/bootstrap.min.css') }}
    </head>
    <body>
        <div class="wrapper">
            <header>
                <h1>Laravel-todo</h1>
                <h2>A simple todo app built with laravel</h2>
                <p>Logged in as {{ $user->email }} - {{ HTML::link_to_action('user@logout', 'Logout') }}</p>
            </header>
            <div role="main" class="main">
                <div class="home">
                    <h2>Your todos</h2>
                    
                    <ul>
                    @foreach ($todos as $todo)
                        <li>
                            {{ $todo->content }}
                            {{ HTML::link_to_action('todo@delete', 'Delete', array($todo->id)) }}
                        </li>
                    @endforeach
                    </ul>
                    
                    <h2>Add new todo</h2>
                    {{ Form::open('todo/add', 'POST', array('class' => 'form-horizontal')) }}
                    <p>
                        {{ Form::text('todo') }}
                    </p>
                    <p>
                        {{ Form::submit('Add', array('class' => 'btn')) }}
                    </p>
                    {{ Form::close() }}

                    <ul class="out-links">
                        <li><a href="http://laravel.com">Official Website</a></li>
                        <li><a href="http://forums.laravel.com">Laravel Forums</a></li>
                        <li><a href="http://github.com/laravel/laravel">GitHub Repository</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </body>
</html>
